const functions = require("firebase-functions");
const jsonServer = require("json-server");

const main = jsonServer.create();
const api = jsonServer.create();
const router = jsonServer.router("db.json", { foreignKeySuffix: "Id" });
const middlewares = jsonServer.defaults();

router.render = function (req, res) {
  var statusCode = res.statusCode;
  var json = {};

  if (statusCode < 400) {
    json.body = res.locals.data;
  }
  json.status = { code: statusCode };
  res.jsonp(json);
};

api.use(middlewares);
api.use(
  jsonServer.rewriter({
    "/": "/",
  })
);
api.use(router);

main.use("/api", api);

exports.main = functions.https.onRequest(main);
