import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { RegistrationFormComponent } from './registration/registration-form/registration-form.component';
import { InputModule } from '../shared/components/input/input.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PhoneModule } from '../shared/directives/phone/phone.module';
import { MessageSuccessfullyModule } from '../shared/components/message-successfully/message-successfully.module';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
    LoginFormComponent,
    RegistrationFormComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    InputModule,
    ReactiveFormsModule,
    FormsModule,
    PhoneModule,
    MessageSuccessfullyModule,
  ],
})
export class AuthModule {}
