import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegistrationFormValue } from './registration-form-value';
import { RegexpPatterns } from '../../../shared/consts/regexp-patterns';

export class RegistrationFormGroup extends FormGroup {
  constructor(registrationFormValue: RegistrationFormValue = null) {
    super({
      name: new FormControl(
        registrationFormValue && registrationFormValue.name
          ? registrationFormValue.name
          : null,
        [Validators.required]
      ),
      nickname: new FormControl(
        registrationFormValue && registrationFormValue.nickname
          ? registrationFormValue.nickname
          : '',
        [Validators.required]
      ),
      email: new FormControl(
        registrationFormValue && registrationFormValue.email
          ? registrationFormValue.email
          : '',
        [Validators.required, Validators.email]
      ),
      phone: new FormControl(
        registrationFormValue && registrationFormValue.phone
          ? registrationFormValue.phone
          : '',
        [Validators.required, Validators.pattern(RegexpPatterns.regExpPhone)]
      ),
      password: new FormControl(
        registrationFormValue && registrationFormValue.password
          ? registrationFormValue.password
          : '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(RegexpPatterns.regExpPassword),
        ]
      ),
    });
  }
}
