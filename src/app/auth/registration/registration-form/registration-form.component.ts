import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator,
} from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { RegistrationFormGroup } from './registration-form-group';
import { RegistrationFormValue } from './registration-form-value';
import { takeUntil } from 'rxjs/operators';

export const REGISTRATION_FORM_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RegistrationFormComponent),
  multi: true,
};

export const REGISTRATION_FORM_VALIDATORS: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => RegistrationFormComponent),
  multi: true,
};

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
  providers: [REGISTRATION_FORM_VALUE_ACCESSOR, REGISTRATION_FORM_VALIDATORS],
})
export class RegistrationFormComponent
  implements OnInit, OnDestroy, ControlValueAccessor, Validator {
  public registrationForm: RegistrationFormGroup;
  public isShowPassword: boolean;
  private unsubscribe: Subject<void>;

  public onChange: any = () => {};
  public onTouched: any = () => {};

  constructor() {
    this.isShowPassword = false;
    this.unsubscribe = new Subject();
  }

  get value(): RegistrationFormValue {
    return this.registrationForm.getRawValue();
  }

  set value(value: RegistrationFormValue) {
    this.registrationForm.setValue(value);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngOnInit(): void {
    this.registrationForm = new RegistrationFormGroup();
    this.valueFormChanges();
  }

  public writeValue(value: RegistrationFormValue): void {
    if (value) {
      this.value = value;
    }
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.registrationForm.disable();
    }
  }

  private valueFormChanges(): Subscription {
    return this.registrationForm.valueChanges
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => {
        this.onChange(this.value);
      });
  }

  public validate(_: FormControl): any {
    return this.registrationForm.valid ? null : { valid: false };
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public toggleShowPassword(): void {
    this.isShowPassword = !this.isShowPassword;
  }
}
