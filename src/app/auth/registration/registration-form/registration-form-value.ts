export class RegistrationFormValue {
  name: string;
  nickname: string;
  email: string;
  phone: string;
  password: string;
}
