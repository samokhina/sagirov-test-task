import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
import { UserService } from '../shared/services/user.service';
import { User } from '../shared/models/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnDestroy {
  public registration: FormControl;
  public isCheck: FormControl;
  public isShowMessageSuccessfully: boolean;

  private readonly unsubscribe: Subject<void>;

  constructor(private router: Router, private userService: UserService) {
    this.registration = new FormControl();
    this.isCheck = new FormControl();
    this.isShowMessageSuccessfully = false;
    this.unsubscribe = new Subject();
  }

  public register(): void {
    this.createUser().subscribe((createdUser: User) => {
      if (createdUser) {
        this.isShowMessageSuccessfully = true;
        setTimeout(() => {
          this.isShowMessageSuccessfully = false;
          this.login();
        }, 3500);
      }
    });
  }

  public login(): void {
    this.router.navigate(['/auth/login']);
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private createUser(): Observable<User> {
    return this.userService
      .createUser(plainToClass(User, this.registration.value))
      .pipe(takeUntil(this.unsubscribe));
  }
}
