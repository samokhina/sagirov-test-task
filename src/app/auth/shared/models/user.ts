export class User {
  constructor(
    public email: string,
    public phone: string,
    public password: string,
    public nickname: string,
    public name: string,
    public id?: number
  ) {}
}
