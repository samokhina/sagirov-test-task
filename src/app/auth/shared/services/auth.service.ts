import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user: BehaviorSubject<User>;
  private isAuthorized: BehaviorSubject<boolean>;

  constructor() {
    this.user = new BehaviorSubject(null);
    this.isAuthorized = new BehaviorSubject(false);
  }

  get authorized(): Observable<boolean> {
    return this.isAuthorized.asObservable();
  }

  public activeUser(): Observable<User> {
    return this.user.asObservable().pipe(delay(1000));
  }

  public login(user: User): void {
    this.user.next(user);
    this.isAuthorized.next(true);
  }

  public logout(): void {
    this.user.next(null);
    this.isAuthorized.next(false);
  }
}
