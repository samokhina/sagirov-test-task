import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
import { BaseService } from '../../../shared/services/base.service';
import { User } from '../models/user';
import { environment } from '../../../../environments/environment';
import { Md5 } from 'ts-md5/dist/md5';
import { getHash } from '../../../shared/functions/get-hash.function';

declare var Chance: any;

@Injectable({
  providedIn: 'root',
})
export class UserService extends BaseService {
  private url = environment.apiURL + 'users';

  constructor(public http: HttpClient) {
    super(http);
  }

  createUser(createUser: User): Observable<User> {
    const chance = new Chance();
    createUser.id = chance.guid();
    createUser.password = getHash(createUser.password);
    return this.post(this.url, createUser).pipe(
      map((response) => {
        if (response && response.body) {
          return plainToClass<any, any>(User, response.body);
        } else {
          return null;
        }
      })
    );
  }

  getUserByEmail(email: string): Observable<User> {
    const params = {
      email: email,
    };
    return this.get(this.url, null, params).pipe(
      map((response) => {
        if (response && response.body && response.body.length) {
          return plainToClass<any, any>(User, response.body[0]);
        } else {
          return null;
        }
      })
    );
  }

  getUserByPhone(phone: string): Observable<User> {
    const params = {
      phone: phone,
    };
    return this.get(this.url, null, params).pipe(
      map((response) => {
        if (response && response.body && response.body.length) {
          return plainToClass<any, any>(User, response.body[0]);
        } else {
          return null;
        }
      })
    );
  }
}
