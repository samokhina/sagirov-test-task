import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginFormValue } from './login-form-value';
import { RegexpPatterns } from '../../../shared/consts/regexp-patterns';

export class LoginFormGroup extends FormGroup {
  constructor(loginFormValue: LoginFormValue = null) {
    super({
      emailPhone: new FormControl(
        loginFormValue && loginFormValue.emailPhone
          ? loginFormValue.emailPhone
          : '',
        [
          Validators.required,
          Validators.pattern(RegexpPatterns.regExpPhoneEmail),
        ]
      ),
      password: new FormControl(
        loginFormValue && loginFormValue.password
          ? loginFormValue.password
          : '',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(RegexpPatterns.regExpPassword),
        ]
      ),
    });
  }
}
