import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import {
  ControlValueAccessor,
  Validator,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';
import { LoginFormGroup } from './login-form-group';
import { LoginFormValue } from './login-form-value';

export const LOGIN_FORM_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => LoginFormComponent),
  multi: true,
};

export const LOGIN_FORM_VALIDATORS: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => LoginFormComponent),
  multi: true,
};

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  providers: [LOGIN_FORM_VALUE_ACCESSOR, LOGIN_FORM_VALIDATORS],
})
export class LoginFormComponent
  implements OnInit, OnDestroy, ControlValueAccessor, Validator {
  public loginForm: LoginFormGroup;
  public isShowPassword: boolean;
  private unsubscribe: Subject<void>;

  public onChange: any = () => {};
  public onTouched: any = () => {};

  constructor() {
    this.isShowPassword = false;
    this.unsubscribe = new Subject();
  }

  get value(): LoginFormValue {
    return this.loginForm.getRawValue();
  }

  set value(value: LoginFormValue) {
    this.loginForm.setValue(value);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngOnInit(): void {
    this.loginForm = new LoginFormGroup();
    this.valueFormChanges();
  }

  public writeValue(value: LoginFormValue): void {
    if (value) {
      this.value = value;
    }
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.loginForm.disable();
    }
  }

  private valueFormChanges(): Subscription {
    return this.loginForm.valueChanges.subscribe(() => {
      this.onChange(this.value);
    });
  }

  public validate(_: FormControl): any {
    return this.loginForm.valid ? null : { valid: false };
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public toggleShowPassword(): void {
    this.isShowPassword = !this.isShowPassword;
  }
}
