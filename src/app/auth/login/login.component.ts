import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RegexpPatterns } from '../../shared/consts/regexp-patterns';
import { getHash } from '../../shared/functions/get-hash.function';
import { AuthService } from '../shared/services/auth.service';
import { UserService } from '../shared/services/user.service';
import { User } from '../shared/models/user';
import { LoginFormValue } from './login-form/login-form-value';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnDestroy {
  public loginValue: FormControl;
  public isShowMessageSuccessfully: boolean;

  private readonly unsubscribe: Subject<void>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ) {
    this.loginValue = new FormControl();
    this.isShowMessageSuccessfully = false;
    this.unsubscribe = new Subject();
  }

  public login(): void {
    if (this.loginValue.valid) {
      const value: LoginFormValue = this.loginValue.value;
      const isEmail: boolean = RegexpPatterns.regExpEmail.test(
        value.emailPhone
      );

      const getUser$: Observable<User> = isEmail
        ? this.getUserByEmail(value.emailPhone)
        : this.getUserByPhone(value.emailPhone);

      getUser$.subscribe((user: User) => {
        if (user) {
          if (user.password === getHash(value.password)) {
            this.isShowMessageSuccessfully = true;
            setTimeout(() => {
              this.isShowMessageSuccessfully = false;
              this.authService.login(user);
              this.router.navigate(['/']);
            }, 3500);
          }
        }
      });
    }
  }

  public register(): void {
    this.router.navigate(['/auth/registration']);
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private getUserByEmail(email: string): Observable<User> {
    return this.userService
      .getUserByEmail(email)
      .pipe(takeUntil(this.unsubscribe));
  }

  private getUserByPhone(phone: string): Observable<User> {
    return this.userService
      .getUserByPhone(phone)
      .pipe(takeUntil(this.unsubscribe));
  }
}
