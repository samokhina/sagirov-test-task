import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AuthService } from '../auth/shared/services/auth.service';
import { User } from '../auth/shared/models/user';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, OnDestroy {
  public loading$: Observable<boolean>;
  public activeUser$: Observable<User>;

  private readonly unsubscribe: Subject<void>;
  private loading: BehaviorSubject<boolean>;

  constructor(private router: Router, private authService: AuthService) {
    this.unsubscribe = new Subject();
    this.loading = new BehaviorSubject(true);
  }

  ngOnInit() {
    this.loading$ = this.loading
      .asObservable()
      .pipe(takeUntil(this.unsubscribe));

    this.activeUser$ = this.authService.activeUser().pipe(
      takeUntil(this.unsubscribe),
      map((user: User) => {
        this.loading.next(false);
        return user;
      })
    );
  }

  public logout(): void {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
