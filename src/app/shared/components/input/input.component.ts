import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent implements OnInit, OnDestroy {
  @Input() public label: string;
  @Input() public type: string;

  @Input()
  public set setFormControl(value: FormControl) {
    this.control = value;
  }

  @Output() change: EventEmitter<FormControl>;

  public control: FormControl;

  private unsubscribe: Subject<void>;

  constructor() {
    this.label = '';
    this.unsubscribe = new Subject();
    this.change = new EventEmitter<FormControl>();
  }

  public ngOnInit(): void {
    this.valueControlChanges();
  }

  private valueControlChanges(): Subscription {
    return this.control.valueChanges
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(() => {
        this.change.emit(this.control);
      });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
