import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageSuccessfullyComponent } from './message-successfully.component';

@NgModule({
  declarations: [MessageSuccessfullyComponent],
  imports: [CommonModule],
  exports: [MessageSuccessfullyComponent],
})
export class MessageSuccessfullyModule {}
