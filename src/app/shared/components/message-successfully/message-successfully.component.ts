import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-message-successfully',
  templateUrl: './message-successfully.component.html',
  styleUrls: ['./message-successfully.component.scss'],
})
export class MessageSuccessfullyComponent {
  @Input() public message: string;

  constructor() {
    this.message = '';
  }
}
