export class RegexpPatterns {
  static regExpPassword: RegExp = /(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]/g;
  static regExpPhone: RegExp = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/g;
  static regExpEmail: RegExp = /^([A-Za-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3})$/g;
  static regExpPhoneEmail: RegExp = /^(((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10})|([A-Za-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3})$/;
}
