import { Md5 } from 'ts-md5';

export function getHash(password: string): string {
  const md5 = new Md5();
  const hash: string | Int32Array = md5.appendStr(password).end();
  return typeof hash === 'string' ? hash : password;
}
