import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';

class HttpOptions {
  headers: HttpHeaders;
  params?: HttpParams;
}

export class BaseService {
  constructor(public httpClient: HttpClient) {}

  private options(headers?: object, params?: object): object {
    const options: HttpOptions = new HttpOptions();
    options.headers = this.headers(headers);
    if (params) {
      options.params = this.params(params);
    }
    return options;
  }

  private params(params?: object): HttpParams {
    let httpParams: HttpParams = new HttpParams();
    for (const param in params) {
      httpParams = httpParams.append(param, params[param]);
    }
    return httpParams;
  }

  private headers(headers?: object): HttpHeaders {
    let httpHeaders: HttpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.append('Content-Type', 'application/json');
    httpHeaders = httpHeaders.append('Accept', 'application/json');
    if (headers) {
    }
    return httpHeaders;
  }

  public get = (
    url: string,
    headers?: object,
    params?: object
  ): Observable<any> => {
    return this.httpClient.get(url, this.options(headers, params));
  };

  public post = (
    url: string,
    item: any,
    headers?: object,
    params?: object
  ): Observable<any> => {
    const body: string = JSON.stringify(item);
    return this.httpClient.post(url, body, this.options(headers, params));
  };

  public put = (
    url: string,
    item: any,
    headers?: object,
    params?: object
  ): Observable<any> => {
    const body: string = JSON.stringify(item);
    return this.httpClient.put(url, body, this.options(headers, params));
  };

  public delete = (
    url: string,
    headers?: object,
    params?: object
  ): Observable<any> => {
    return this.httpClient.delete(url, this.options(headers, params));
  };
}
