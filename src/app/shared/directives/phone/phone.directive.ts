import { Directive, ElementRef, AfterViewInit } from '@angular/core';
import * as Inputmask from 'inputmask';

@Directive({
  selector: '[appPhone]',
})
export class PhoneDirective implements AfterViewInit {
  private phoneMask = '+7 999 999 99 99';
  private phone: HTMLInputElement;

  constructor(private element: ElementRef) {}

  ngAfterViewInit(): void {
    this.phone = this.element.nativeElement.getElementsByTagName('input')[0];
    const im: Inputmask = new Inputmask({
      mask: this.phoneMask,
      showMaskOnHover: false,
      showMaskOnFocus: true,
    });
    im.mask(this.phone);
  }
}
