import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneDirective } from './phone.directive';

@NgModule({
  declarations: [PhoneDirective],
  imports: [CommonModule],
  exports: [PhoneDirective],
})
export class PhoneModule {}
